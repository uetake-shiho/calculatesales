package jp.alhinc.uetake_shiho.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;



public class CalculateSales {
	public static void main(String[] args) {
		Map<String, String> nameMap = new TreeMap<String, String>();
		Map<String, Long> saleMap = new TreeMap<String, Long>();
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		String branchName = "支店";
		String branchLineCheck = "^\\d{3}$";
		if(!inPut(args[0],branchName,branchLineCheck,nameMap,saleMap)) {
			return;
		}
		ArrayList<File> saleLists = new ArrayList<File>();
		String path = args[0];
		File dir = new File(path);
		File[] files = dir.listFiles();
		String saleListName;
		String saleCheck;
		int check = 1;
		for (int i = 0; i < files.length; i++) {
			String saleName = files[i].getName();
			if (files[i].isFile() && saleName.matches("^\\d{8}.rcd$")){
				saleLists.add(files[i]);
			}
		}
		for (int i = 0; i < saleLists.size(); i++) {
			saleListName = saleLists.get(i).getName();
			saleCheck = saleListName.substring(1, 8);
			int saleCheckName = Integer.parseInt(saleCheck);
			if(saleCheckName != check) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}else {
				check++;
			}
		}
		for (int i = 0; i < saleLists.size(); i++) {
			ArrayList<String> saleList = new ArrayList<String>();
			BufferedReader rcdFile = null;
			try{
				String rcdFileName = (saleLists.get(i)).getName();
				rcdFile = new BufferedReader(new FileReader(saleLists.get(i)));
				String saleFile;
				while((saleFile = rcdFile.readLine()) != null) {
					saleList.add(saleFile);
				}
				if(saleList.size() != 2){
					System.out.println( rcdFileName + "のフォーマットが不正です");
					return;
				}
				if(!saleMap.containsKey(saleList.get(0))){
					System.out.println( rcdFileName + "の支店コードが不正です");
					return;
				}
				if (!saleList.get(1).matches("\\d+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long saleAmount;
				int saleAmountLen;
				long saleListGet = Long.parseLong(saleList.get(1));
				saleAmount  = saleMap.get(saleList.get(0)) + saleListGet;
				saleAmountLen = String.valueOf(saleAmount).length();
				if(saleAmountLen > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				saleMap.put(saleList.get(0), saleAmount);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if( rcdFile != null) {
					try {
						rcdFile.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		String outFileName = "branch.out";
		if(!outPut(args[0], outFileName,nameMap,saleMap)) {
			return;
		}

	}
	public static boolean inPut(String pathFile,String branchName,String branchLineCheck,Map<String, String> nameMap,Map<String, Long> saleMap) {
		String code;
		long sale;
		String name;
		BufferedReader br = null;
		try {
			File file = new File(pathFile,"branch.lst") ;
			if(file.exists()) {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
			}else{
				System.out.println(branchName + "定義ファイルが存在しません");
				return false;
			}
			String line;
			while((line = br.readLine()) != null) {
				String branchLine[] = line.split(",");
				if( branchLine.length == 2 && branchLine[0].matches(branchLineCheck)) {
					code = branchLine[0];
					name = branchLine[1];
					sale = 0;
					nameMap.put(code, name);
					saleMap.put(code, sale);

				}else {
					System.out.println(branchName + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}return true;
	}
	public static boolean outPut(String pathFile,String fileName,Map<String, String> nameMap,Map<String, Long> saleMap) {
		File newfile = new File(pathFile,fileName);
		BufferedWriter banchOutFile = null;
		try {
			newfile.createNewFile();
			banchOutFile = new BufferedWriter(new FileWriter(newfile));
			for(Map.Entry<String, String>nameentry : nameMap.entrySet()) {
				for(Map.Entry<String, Long>saleentry : saleMap.entrySet()) {
					if(nameentry.getKey() == saleentry.getKey()) {
						banchOutFile.write(nameentry.getKey() + "," + nameentry.getValue() + "," + saleentry.getValue());
						banchOutFile.newLine();
					}
				}
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if( banchOutFile != null) {
				try {
					banchOutFile.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
